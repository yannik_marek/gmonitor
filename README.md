# Building from Source:
### Configuring with CMake
Compile and install gmonitor in /usr/local/bin
``` bash
$ cd gmonitor
$ mkdir build
$ cd build
$ cmake ..
$ make
$ sudo make install
```

# Usage:
Download the executable from the latest release or compile your own from the source code in the folder "src", run the executable with the following arguments:

-d [displayMode]<br />
0 (default) Monitor both current and previous GPU states.<br />
1 Monitor most recent GPU state only.<br />
2 Monitor previous GPU states only.<br />
3 Same as 0, print current states for all GPU then print history.<br />

-r [refreshRate]<br />
Monitoring refresh rate (default is 2 seconds). <br />

-h<br />
Display this menu.<br />

