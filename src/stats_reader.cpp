/*
 * Copyright (C) 2015 Mountassir El Hafi, (mountassirbillah1@gmail.com)
 *
 * StatsReader.cpp: Part of gmonitor
 *
 * gmonitor is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * gmonitor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with gmonitor.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "stats_reader.h"
#include <sstream>
#include <string>
#include <cstdlib>
#include <fstream>
#include <thread>
#include <chrono>
#include <atomic>

state_description STATE_DESCRIPTIONS[NUMBER_OF_STATES] = {{true, "GPU", "%", BLUE, 100.0},{true, "FREQ", "MHz", BLUE, 800.0},{false, "VRAM", "%", YELLOW, 100.0},{false, "MBUS", "%", GREEN, 100.0},{false, "PBUS", "%", LBLUE, 100.0},{false, "TEMP", "c", RED},{true, "TEMP1", "c", RED, 120.0},{true, "TEMP2", "c", RED, 120.0}};

double read_value(std::string path)
{
    std::ifstream file(path, std::ios::binary);
    double output = 0;

    if (file.is_open()) {
        file >> output;
        return output;
    }

    return output;
}


static std::atomic<double> gpu_usage;
void perf_read_thread()
{
    while (true) {
        std::ifstream perf_open("/sys/kernel/debug/dri/0/perf");
        if (perf_open.is_open()) {
            for (std::string line; std::getline(perf_open, line); ) {
                try {
                gpu_usage = std::stod(line);
                } catch (...) {}
            }
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    }
}

//get the current states of a particular gpu
void StatsReader::getGpuStates(GpuStates *gpuStates,
            		           const string &gpuId)
{
    static std::thread perf_thread(perf_read_thread);

// 	string command;
// 
// 	getAllGpuStatesCommand(gpuId, &command);
// 
// 	std::vector<double> values;
// 
// 	if(!getDoubleFromSystemCall(command, &values) || (values.size() != NUMBER_OF_SUPPORTED_STATES))
// 	{
// 		return;
// 	}

	//read_value("");
// 	gpuStates->gpuUsage        = 0.0;
// 	gpuStates->memoryBandwidth = 0.0;
// 	gpuStates->pciBandwidth    = 0.0;
// 	gpuStates->totalMemory     = 0.0;
// 	gpuStates->usedMemory      = 0.0;
// 	gpuStates->coreTemp        = 0.0;

	//calculate memory usage percentage
// 	gpuStates->memoryUsage     = 0.0;//(gpuStates->usedMemory / gpuStates->totalMemory) * 100;
    gpuStates->states[FREQ] = read_value("/sys/devices/platform/soc@0/3d00000.gpu/devfreq/3d00000.gpu/cur_freq") / 1000000.0;
    gpuStates->states[TEMP1] = read_value("/sys/class/thermal/thermal_zone24/temp") / 1000.0;
    gpuStates->states[TEMP2] = read_value("/sys/class/thermal/thermal_zone25/temp") / 1000.0;
    gpuStates->states[GPU] = gpu_usage;

    STATE_DESCRIPTIONS[FREQ].divisor = read_value("/sys/devices/platform/soc@0/3d00000.gpu/devfreq/3d00000.gpu/max_freq") / 1000000.0;
}

//execute a bash command and return the output as double
bool StatsReader::getDoubleFromSystemCall(string &command, std::vector<double> *values)
{
	FILE *in;
	char buff[512];

	command = checkIfSshCommand(command);

	if(!(in = popen(command.c_str(), "r")))
	{
		return false;
	}

	while(fgets(buff, sizeof(buff), in) != NULL)
	{
		values->push_back(atoi(string(buff).c_str()));

	}

	pclose(in);

	return true;
}

//get the list of available gpus on this machine
bool StatsReader::getGpuList(vector<string> *gpuList)
{
    /*
	FILE *in;
	char buff[512];

	string command;

	//get the bash command
	gpuListCommand(&command);

	command = checkIfSshCommand(command);

	//execute bash command
	if(!(in = popen(command.c_str(), "r")))
	{
		return false;
	}

	//while the line starts with '[', push a new gpu
	//ie: "[0] mountassir-workstation:0[gpu:0] (GeForce GTX 770)"
	while(fgets(buff, sizeof(buff), in) != NULL)
	{
		if(buff[0] == '[')
		{
			gpuList->push_back(buff);
		}
	}

	pclose(in);
	*/

    gpuList->push_back("[0] localhost.localdomain:0[gpu:0] ()");
	return true;
}

//=======================================================================
//         functions returning the appropriate bash commands
//=======================================================================

//get all list of available gpus
//and trim any leading spaces (sed -e 's/^ *//')
void StatsReader::gpuListCommand(string *gpuList)
{
	stringstream oss;
	oss << "nvidia-settings ";
	if(_optirun)
		oss << "-c :8 ";
	oss << "-t -q gpus | sed -e 's/^ *//'";

 	*gpuList = oss.str();
}

//get current gpu states, gpuId = [gpu:0], [gpu:1]...
//split results by ',' (tr ',' '\n')
//grep only numbers in the line (sed 's/[^0-9]//g')
void StatsReader::getAllGpuStatesCommand(const string &gpuId, string *command)
{
	stringstream oss;

	oss << "nvidia-settings ";
	if(_optirun)
		oss << "-c :8 ";
	oss << "-t -q " << gpuId << "/GPUUtilization -q "
	    << gpuId << "/GPUCoreTemp -q " 
	    << gpuId << "/TotalDedicatedGPUMemory -q "
	    << gpuId << "/UsedDedicatedGPUMemory "
	    << " | tr ',' '\n' | sed 's/[^0-9]//g'";

	*command = oss.str();
}

void StatsReader::useOptirun(const bool withOptirun)
{
	_optirun = withOptirun;
}

string StatsReader::checkIfSshCommand(string &command)
{
	if(_overSsh)
	{
		return "DISPLAY=:0 XAUTHORITY=/var/run/lightdm/root/:0 " + command;
	}

	return command;
}
